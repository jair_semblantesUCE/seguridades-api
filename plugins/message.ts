import { Plugin } from '@nuxt/types'

declare module 'vue/types/vue' {
  interface Vue {
    $message: {
      showSnackbar ({ content, color }: { content: string; color: string }) :void;
    };
  }
}

const messagePlugin: Plugin = ({ store }, inject) => {
  inject('message', {
    showSnackbar ({ content = '', color = '' }) {
      store.commit('snackbar/showSnackbar', { content, color })
    },
  })
}

export default messagePlugin
