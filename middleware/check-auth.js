export default async function ({ store, route, redirect }) {
  const { path } = route
  if (path.startsWith('/administrator')) {
    if (await store.dispatch('session/check')) {
      if (path.includes('/login')) {
        redirect('/administrator')
      }
    } else if (!path.includes('/login')) {
      redirect('/administrator/login')
    }
  }
}
