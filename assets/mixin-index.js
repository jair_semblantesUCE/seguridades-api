import debounce from 'debounce'
import moment from 'moment'

const $limit = 10

export default {
  data () {
    const $page = parseInt(this.$route.query.$page) || 1
    const $q = this.$route.query.$q
    return {
      data: [],
      filter: $q,
      states: [],
      isLoadingTable: false,
      params: { $q, $page, $limit },
      pagination: {
        itemsLength: 0,
        itemsPerPage: $limit,
        page: $page,
        total: 0,
      },
      localeTable: {
        filterConfirm: 'Aceptar',
        filterReset: 'Limpiar',
        emptyText: 'No existen datos',
      },
    }
  },
  watch: {
    pagination: {
      handler () {
        this.changeTable()
      },
      deep: true,
    },
  },
  mounted () {
    this.init()
  },
  methods: {
    getDate (date) {
      return moment(date).format('DD-MM-YYYY')
    },
    totalPages (a, b) {
      const res = a % b
      const total = (a / b)
      if (res !== 0) {
        return parseInt(total + 1)
      }
      return total
    },
    doFilter: debounce(function () {
      this.params.$q = this.filter !== '' ? this.filter : undefined
      const query = { ...this.$router.query, $q: this.params.$q }
      this.params.$page = 1
      if (this.params.$q === undefined) { delete query.$q }
      this.$router.push({ query }, this.init)
    }, 300),
    changeTable () {
      const query = { ...this.$route.query }
      const page = this.pagination.page
      if (page !== this.params.page) {
        query.$page = page
        if (query.$page === 1) { delete query.$page }
        this.params.$page = page
      }
      // query.$page = page
      /* for (const key in filters) {
        const filter = filters[key].join(',')
        if (filter) {
          this.params[key] = query[key] = filter
        } else {
          delete query[key]
          delete this.params[key]
        }
      } */
      this.$router.push({ query }, this.init)
    },
    async init () {
      try {
        this.isLoadingTable = true
        this.data = await this.$axios.$get(this.endpoint)
      } catch (err) {
        const message = err.response ? err.response.data.message : err.message
        this.$error({
          title: 'Error al cargar los datos',
          content: message || 'No se pudo recibir los datos',
        })
      } finally {
        this.isLoadingTable = false
      }
    },
  },
}
