export async function b64toblob (base64Data: string) {
  const base64Response = await fetch(`data:application/pdf;base64,${base64Data}`)
  const blob = await base64Response.blob()
  return blob
}
export function filetob64 (file:any) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result?.toString().replace('data:', '').replace(/^.+,/, ''))
    reader.onerror = error => reject(error)
  })
}

export function dataURLtoFile (dataurl: any, filename: string) {
  const arr = dataurl.split(',')
  const mime = arr[0].match(/:(.*?);/)[1]
  const bstr = atob(arr[1])
  let n = bstr.length
  const u8arr = new Uint8Array(n)

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }

  return new File([u8arr], filename, { type: mime })
}
