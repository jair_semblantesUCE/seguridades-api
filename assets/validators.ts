export function required (customMessage = 'Este campo es requerido') {
  return (value: string | number) => {
    return (!!value && value !== '' && typeof value !== 'undefined') || customMessage
  }
}
export function typeId (customMessage = 'Es necesario ingresar una identificación válida, recuerde 10 dígitos para cédula y 13 para RUC') {
  return (value: string) => {
    return (value.length === 10 || value.length === 13) || customMessage
  }
}
export function onlyNumbers (customMessage = 'Es necesario ingresar solo números') {
  return (value: string) => {
    return (/^([0-9])*$/.test(value)) || customMessage
  }
}
export function typeSign (customMessage = 'El documento seleccionado no corresponde a una firma electronica válida') {
  return (value: File) => {
    return (!value || value.type === 'application/x-pkcs12') || customMessage
  }
}

export function email (customMessage = 'El email ingresado no es válido') {
  return (value: string) => {
    return (!!value && /.+@.+\..+/.test(value)) || customMessage
  }
}
