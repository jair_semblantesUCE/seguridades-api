export function downloadHelper (file, filename) {
  // const { userAgent } = window.navigator
  const blob = new File([file], filename, { type: file.type })
  const urlEncode = window.URL.createObjectURL(blob)
  const linkElement = window.document.createElement('a')
  linkElement.addEventListener('click', () => {
    setTimeout(() => window.URL.revokeObjectURL(urlEncode), 100)
  })
  linkElement.href = urlEncode
  // if (/Firefox/.test(userAgent) && filename.includes('pdf')) {
  //   linkElement.target = '_blank'
  // } else {
  //   linkElement.download = filename
  // }
  linkElement.download = filename
  linkElement.click()
}
