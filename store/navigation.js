export const state = () => ({
  current: 0,
})

export const mutations = {
  next (state) {
    state.current++
  },
  prev (state) {
    state.current--
  },
  payment (state) {
    state.current = 2
  },
  set (state, data) {
    state.current = data
  },
}
