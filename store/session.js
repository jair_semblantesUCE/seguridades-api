
export const state = () => ({
  AdministratorToken: undefined,
  AdministratorData: undefined,
  isLogin: false,
  user: 'admin',
  password: '12345a',
})

const Cookies = {
  Administrator: 'as24ad',
}
export const mutations = {
  setToken (state, token) {
    this.$axios.setToken(token)
    state.AdministratorToken = token
  },
  setData (state, data) {
    state.AdministratorData = data
  },
  logIn () {
    this.$cookies.set(Cookies.Administrator, true)
  },
  logOut () {
    this.$cookies.remove(Cookies.Administrator)
  },
}

export const actions = {
  check () {
    const token = this.$cookies.get(Cookies.Administrator)
    console.log(token)
    if (!token) { return false }
    return true
  },
}
