import { GetterTree, MutationTree } from 'vuex'

export const state = () => ({
  content: '',
  color: '',
  projectedrevenue: 0,
})
export const getters: GetterTree<RootState, RootState> = {
  content: state => state.content,
  color: state => state.color,
  projectedrevenue: state => state.projectedrevenue,
}
export type RootState = ReturnType<typeof state>
export const mutations: MutationTree<RootState> = {
  showSnackbar (state, data) {
    state.content = data.content
    state.color = data.color
  },
  changeprojectedrevenue (state, data) {
    state.projectedrevenue = data
  },
}
