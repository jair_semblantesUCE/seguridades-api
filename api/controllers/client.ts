/* eslint-disable @typescript-eslint/no-unused-vars */
import fs from 'fs'
import path from 'path'
import { Request, Response, NextFunction } from 'express'
import ClientService from '../services/Client'

const clientService = new ClientService()

export default {
  async getStats (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.getStats())
    } catch (error) {
      next(error)
    }
  },
  getFile (req: Request, res: Response, next: NextFunction) {
    try {
      const file = path.resolve(__dirname, '../assets/Programa Cientifico SEOT 2021.pdf')
      const pdfBuffer = fs.readFileSync(file)
      const len = pdfBuffer.length
      res.header('Content-Disposition', `filename=Programa Cientifico SEOT 2021.pdf`)
      res.header('Content-Length', len.toString())
      res.header('Content-Type', 'application/pdf')
      res.write(pdfBuffer)
      res.end()
      next()
    } catch (err) { next(err) }
  },
  async rebilling (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.rebilling(req.body))
    } catch (error) {
      next(error)
    }
  },
  async savedPaymentCard (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.savedPaymentCard(req.params.id, req.body))
    } catch (error) {
      next(error)
    }
  },
  async verifiedPaymentCard (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.verifiedPaymentCard(req.params.id))
    } catch (error) {
      next(error)
    }
  },
  async changeStusPayment (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.changeStusPayment(req.params.id, req.body, false))
    } catch (error) {
      next(error)
    }
  },
  async clientReport (req: Request, res: Response, next: NextFunction) {
    try {
      const xlsx = await clientService.clientReport(req.query)
      res.header('Content-Disposition', xlsx.name)
      res.header('Content-Type', 'application/xlsx')
      res.header('Content-Length', xlsx.data.length)
      res.write(xlsx.data)
      res.end()
    } catch (error) {
      next(error)
    }
  },
  async reversePayment (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.reversePayment(req.body))
    } catch (error) {
      next(error)
    }
  },
  async createPaymentLink (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(await clientService.createPaymentLink(req.body.data, req.body.client))
    } catch (error) {
      next(error)
    }
  },
  async post (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = await clientService.post(req.body)
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },
  async update (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = await clientService.update(req.params.id, req.body)
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },

  async get (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = await clientService.get(req.query)
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },
  async delete (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = await clientService.delete(req.params.id)
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },
  async getOne (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = await clientService.getOne(req.params.id)
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getOptions (req: Request, res: Response, next: NextFunction) {
    try {
      const resp = clientService.getOptions()
      res.json(resp)
    } catch (error) {
      next(error)
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async reSendEmail (req: Request, res: Response, next: NextFunction) {
    try {
      const [send] = await clientService.reSendEmail(req.body)
      if (send.statusCode >= 400) {
        throw new Error('No se pudo enviar el correo')
      }
      res.json({ message: 'success' })
    } catch (error) {
      next(error)
    }
  },
}
