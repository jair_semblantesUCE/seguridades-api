import { Request, Response, NextFunction } from 'express'
import AdminService from '../services/Admin'

const adminService = new AdminService()

export default {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getOptions (req: Request, res: Response, next: NextFunction) {
    try {
      res.json(adminService.getOptions())
    } catch (error) {
      next(error)
    }
  },
}
