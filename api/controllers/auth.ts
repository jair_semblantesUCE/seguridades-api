import { Request, Response, NextFunction } from 'express'
import moment from 'moment'
import jwt from 'jwt-simple'
import Administrator from '../services/Auth'
import Admin from '../models/Administrators'

const administrator = new Administrator()
const SECRET = 'sriTesT#asd123a#$%&'

export const adminMiddleware = middleware(SECRET)

function middleware (secret: string) {
  return async (req: any, _res: Response, next: NextFunction) => {
    try {
      const authorization = getToken(req)
      req.payload = await getPayload(authorization, secret)
      next()
    } catch (err) { next(err) }
  }
}

function createToken (id: string, secret: string, expiration: string, name: string, email: String) {
  const date = moment().toISOString()
  const payload = { id, date, expiration, name, email }
  return jwt.encode(payload, secret)
}
async function getPayload (token: any, secret: string) {
  const payload = jwt.decode(token, secret)
  const admin = await Admin.findOne({ _id: payload.id })
  if (!admin) {
    throw new Error('El administrador no existe')
  }
  if (payload.expiration) {
    const expiration = moment(payload.expiration)
    const remaining = expiration.diff(new Date())
    if (remaining < 0) {
      throw new Error('El token ha expirado')
    }
  }
  return payload
}

function getToken (req: Request) {
  const token = req.header('Authorization') || req.query.token
  if (!token) {
    throw new Error('No se ha proporcionado un token')
  }
  return token
}

export default {
  async login (req: Request, res: Response, next: NextFunction) {
    try {
      const { id, expiration, name, email } = await administrator.auth(req.body)
      res.send(createToken(id, SECRET, expiration, name, email))
    } catch (err) { next(err) }
  },
  async getAuth (req: Request, res: Response, next: NextFunction) {
    try {
      const authorization = getToken(req)
      res.json(await getPayload(authorization, SECRET))
    } catch (err) { next(err) }
  },
}
