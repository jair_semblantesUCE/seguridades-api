function getHTML (data) {
  return `\
  <html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
      <style>
        body, p, div {
          font-family: arial;
          font-size: 14px;
        }
        body {
          color: #000000;
          background: #FFFFFF;
          margin: 0xp;
        }
        a {
          color: #1188E6;
          text-decoration: none;
        }
        .container {
          width: 50%;
          margin: 0px auto;
          padding: 20px;
          background: #FFFFFF;
          justify-content: center; 
          align-items: center;
        }
        .button-success {
          background-color: #042e69;
          border: 1px solid #042e69;
          border-radius: 6px;
          border-width: 1px;
          color: #042e69;
          padding: 12px 18px 12px 18px;
          text-decoration: none;
        }
        @media screen and (max-width:480px) {
          body {
            background: #FFFFFF;
          }
          .container {
            width: auto;
            margin: 0px;
            padding: 10px;
          }
        }
      </style>
    </head>
    <body>
      <div class="container">
        <div style="margin-bottom: 35px; text-align: center;">
          <img src="https://seotecuador.com/wp-content/uploads/Imagen-SEOT-1.jpg" width="300" />
        </div>
        ${data}
      </div>
    </body>
  </html>`
}
export function getClientWelcome (data) {
  const template = getHTML(`\
<div>
  <p>Estimado(a)  <b>${data.name}</b>.</p>
  <p>
    Sociedad Ecuatoriana de Ortopedia y Traumatología, ve con enorme beneplácito su interés</br>
    en el curso Ecuatoriano de artroscopía y cirugía preservadora</br>
    para los días 23 al 25 de junio 2021. A continuación encontrará información</br>
    importante para su acceso el evento online:
  </p>
  <p>
    <b>Documento de identificación:</b> ${data.ci} <br>
    <b>Usuario:</b> ${data.username} <br>
    <b>Contraseña:</b> ${data.password}
    <p><strong>Nota: Acceso a plataforma del evento habilitada a partir del 22 de junio.<br>
    Aplicación requerida en su computador: zoom.
    </strong>
    </p>
  </p>
  <p>Link de conexión al evento:</p>
  
  <div style="text-align:center;padding: 20px 0">
    <a style="color:#FFFFFF" class="button-success" href="https://seotecuador.com/" target="_blank">
      Ir al enlace
    </a>
  </div>

  <p><b>Atentamente,</b></p>
  <p>Directiva Sociedad Ecuatoriana de Ortopedia y Traumatología</p>
</div>`)
  return template
}
