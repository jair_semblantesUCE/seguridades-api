import crypto from 'crypto'
import fs from 'fs'
import path, { join } from 'path'
import XlsxTemplate from 'xlsx-template'
import { Storage } from '@google-cloud/storage'

export const isDev = process.env.DEVELOPMENT === 'true'

const storage = new Storage({
  keyFilename: path.resolve(__dirname, '../assets/gstorage-token.json'),
})
export const bucket =
  storage.bucket(isDev ? 'toc-storage-development' : 'toc-storage')

function generateBuffer (data:object, dirTemplate:string) {
  if (!data && !dirTemplate) {
    throw new Error('No se recibio los datos necesarios, intente nuevamente')
  }
  const templateBuffer = fs.readFileSync(dirTemplate)
  const template = new XlsxTemplate(templateBuffer)
  template.substitute(1, data)
  return template.generate()
}

export function getBufferReport (data:object, path:string) {
  if (!data || !path) {
    throw new Error('Es necesario obtener los datos del reporte, intente nuevamente')
  }
  const dir = join(__dirname, path)
  return (Buffer.from(generateBuffer(data, dir) as any, 'binary'))
}

export function hashThis (text: string) {
  return crypto.createHash('md5').update(text).digest('hex')
}

export function randomString (length = 6) {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}
