/* eslint-disable no-console */
import Debug from 'debug'
import generatePayment, { getStatusLinkPayment, reversePayment } from 'pagomedios-ec'
import { isDev } from '../utils'

const debug = Debug('api:libs:integrations:pagomedios')
const FRONTEND = process.env.FRONTEND || 'http://localhost:3000'
const tokenProd = '2y-13-9h-vflgee6m9b-l-o3zdiuzkzafa8mibbon9wt3jfkggbssctey1ggjkxr6apt2hknvo7kx8b'
async function postCreateLinkPayment (data, client) {
  debug('creando link de pago por %s', data.price)
  try {
    const body = {
      companyType: 'Persona Natural',
      document: data.id,
      documentType: data.typeId,
      fullName: data.fullname,
      address: data.address,
      mobile: data.cellphone,
      email: data.email,
      reference: `${data.id}-${new Date().toLocaleString()}`,
      description: `\
Incripción al curso Ecuatoriano de astroscopía y cirugía preservadora`,
      notifyUrl: `${FRONTEND}?id=${client}`,
      amount: !isDev ? data.price : 1.12,
      amountWithTax: !isDev ? 0 : 1,
      amountWithoutTax: !isDev ? data.price : 0,
      tax: !isDev ? 0 : 0.12,
      gateway: 3,
    }
    console.log(body)
    if (!isDev) {
      return await generatePayment(body, tokenProd)
    }
    return await generatePayment(body)
  } catch (e) {
    console.log(e)
    throw new Error(`Error al crear el link de pago; type: ${e.type}, msg: ${e.message}`)
  }
}

async function statusLinkPayment (id) {
  debug(`consultando estado del link de pago, token: ${id}`)
  try {
    return await getStatusLinkPayment(id)
  } catch (e) {
    console.log(e)
    throw new Error(`El pago aun no se ha completado o ha sido rechazado, \
intentelo nuevamente; type: ${e.type}, error: ${e.message}`)
  }
}

async function reversePaymentLink (data) {
  return await reversePayment(data)
}

export {
  statusLinkPayment,
  postCreateLinkPayment,
  reversePaymentLink,
}
