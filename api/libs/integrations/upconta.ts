import axios from 'axios'

const RUC = process.env.UPCONTA_USER || '1716766033001'
const USERNAME = process.env.UPCONTA_USERNAME || '1716766033001'
const PASSWORD = process.env.UPCONTA_PASSWORD || '54321'
const ENDPOINT =
  process.env.UPCONTA_ENDPOINT || 'https://upconta.uc.r.appspot.com'
const ESTABLISHMENT = process.env.UPCONTA_ESTABLISHMENT || '001'
const EMISSIONPOINT = process.env.UPCONTA_EMISSIONPOINT || '002'

const _up = (path: string) => ENDPOINT + path

/**
 * Utiliza las credenciales proporcionadas para generar un token que pueda
 * acceder a UpConta. Si no se puede acceder se produce un error, por lo que es
 * recomendable que está función sea llamada al iniciar el api de firmas para
 * comprobar la conexión.
 */
export async function generateToken () : Promise<string> {
  const { data } = await axios.post(_up('/auth'), {
    ruc: RUC, username: USERNAME, password: PASSWORD,
  })
  return data.token
}

export interface Client {
  /** El número de identificación de la persona. */
  id: string;
  /**
   * El tipo de identificación: RUC (`'04'`), cédula (`'05'`) o pasaporte
   * (`'06'`).
   */
  idType: '04' | '05' | '06';
  name: string;
  /**
   * El tipo de sujeto: Persona natural (`'01'`) o persona jurídica (`'02'`).
   */
  subjectType: '01' | '02';
  phone: string;
  email: string;
  address: string;
  cellphone: string;
  optionalEmail?: string;
  city: string;
}

/**
 * Obtiene el ID del cliente al cual se le va a emitir la factura. Si este no
 * existe debe ser creado.
 * @param client El cliente que se desea buscar o crear.
 * @param token El token para acceder al api de UpConta.
 */
export async function getClientID (client: Client, token: string) :Promise<string> {
  const headers = { Authorization: token }
  const endpoint = _up('/administrative/clients')
  try {
    const { data: clients } = await axios.get(endpoint, {
      params: { id: client.id, idType: client.idType }, headers,
    })
    if (clients.length !== 0 && clients[0].id === client.id) {
      return clients[0]._id
    }
    const { data: newClient } = await axios.post(endpoint, client, { headers })
    return newClient._id
  } catch (err) {
    const { response: { data } } = err
    throw new Error(`Causa: ${data.message}, No se pudo buscar ni crear al cliente`)
  }
}

/**
 * Obtiene el id del establecimiento y punto de emisión que se van a usar para
 * la emisión de facturación electrónica.
 * @param token Token de acceso al api de UpConta.
 * @param establishmentID El establecimiento. Ejemplo: `'001'`.
 * @param emissionPointID El punto de emisión. Ejemplo: `'002'`.
 */
export async function getEstablishmentAndEmissionPoint (
  token: string,
  establishmentID: string = ESTABLISHMENT,
  emissionPointID: string = EMISSIONPOINT,
) {
  const { data } = await axios.get(_up('/business/me/establishments'), {
    headers: { Authorization: token },
  })
  const establishment =
    data.find((establishment:any) => establishment.id === establishmentID)
  if (!establishment) {
    throw new Error(`No se pudo obtener el establecimiento`)
  }
  const emissionPoint = establishment.emissionPoints.find(
    (emissionPoint:any) => emissionPoint.id === emissionPointID)
  if (!emissionPoint) {
    throw new Error(`No se pudo obtener el punto de emisión`)
  }
  return { establishment: establishment._id, emissionPoint: emissionPoint._id }
}

/**
 * Obtiene el id de producto al que se generará la factura. Si no existe se
 * genera un error.
 * @param code El código de producto.
 * @param token El token de acceso a UpConta.
 */
export async function getProductID (code: string, token: string) :Promise<string> {
  try {
    const { data } = await axios.get(_up('/inventory/services'), {
      params: { code }, headers: { Authorization: token },
    })
    if (data.length !== 0) { return data[0]._id }
    throw new Error('No exite producto')
  } catch {
    throw new Error(`No se pudo obtener el producto ${code}`)
  }
}

type FormasDePago = '01' | '15' | '16' | '17' | '18' | '19' | '20' | '21'

export interface Invoice {
  establishment?: string;
  emissionPoint?: string;
  issueDate?: Date;
  client?: string;
  infoFactura: {
    pagos: [
      {
        /**
         * La forma de pago:
         * - `'01'`: Sin utilizacion del sistema financiero
         * - `'15'`: Compensación de deudas
         * - `'16'`: Tarjeta de débito
         * - `'17'`: Dinero electrónico
         * - `'18'`: Tarjeta prepago
         * - `'19'`: Tarjeta de crédito
         * - `'20'`: Otros con utilizacion del sistema financiero
         * - `'21'`: Endoso de títulos 21
         */
        formaPago: FormasDePago;
      }
    ];
  };
  detalles: [
    {
      product: string;
      descripcion?: string;
      cantidad: number;
      precioUnitario?: number;
      descuento?: number;
    }
  ];
  // Este campo es solo para solventar un bug en UpConta y será quitado
  // posteriormente.
  reembolsos?: any;
}

async function sentInvoice (invoice: Invoice, token: string, {
  type = 'electronic',
} : { type?: 'physical' | 'electronic' } = {}) {
  try {
    let warning
    const axiosArgs = { headers: { Authorization: token }, timeout: 1000 * 60 }
    const endpoint = type === 'physical'
      ? _up('/administrative/physical-invoices')
      : _up('/administrative/invoices')
    const { data } = await axios.post(endpoint, invoice, axiosArgs)
    if (type === 'electronic') {
      await new Promise(resolve => setTimeout(resolve, 1000 * 10))
      try {
        await axios.get(`${endpoint}/${data._id}/status`, axiosArgs)
      } catch (err) {
        warning = err.response ? err.response.data : err.toString()
      }
    }
    return { invoice: data, warning }
  } catch (err) {
    const data = err.response ? err.response.data : err.toString()
    throw new Error(`Causa: ${data.message}, No se pudo generar la factura`)
  }
}

export async function checkStatusInvoice (id: string) {
  try {
    const token = await generateToken()
    const axiosArgs = {
      params: { fields: 'status' },
      headers: { Authorization: token },
      timeout: 6000,
    }
    const endpoint = _up('/administrative/invoices')
    const { data } = await axios.get(`${endpoint}/${id}`, axiosArgs)
    return data
  } catch (err) {
    throw new Error(`No se pudo consultar el estado de la factura`)
  }
}

export default async function (client: Client, {
  formaPago = '19',
  issueDate = new Date(),
  product,
  cantidad = 1,
  precioUnitario,
  descuento = 0,
  descripcion,
} : {
  formaPago?: FormasDePago;
  issueDate?: Date;
  product: string;
  descripcion?: string;
  cantidad?: number;
  precioUnitario: number;
  descuento?: number;
}, { type = 'electronic' } : { type?: 'electronic' | 'physical' } = {}) {
  const token = await generateToken()
  const estab = await getEstablishmentAndEmissionPoint(token)
  const invoice: Invoice = {
    establishment: estab.establishment,
    emissionPoint: estab.emissionPoint,
    issueDate,
    client: await getClientID(client, token),
    infoFactura: { pagos: [{ formaPago }] },
    detalles: [
      {
        product: await getProductID(product, token),
        descripcion,
        cantidad,
        precioUnitario,
        descuento,
      },
    ],
    reembolsos: [],
  }
  return await sentInvoice(invoice, token, { type })
}
