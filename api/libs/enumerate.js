class Enumerate {
  constructor (data) {
    this.data = data
  }

  getEnum () {
    return this.data.map(x => x.id)
  }

  getData () {
    return this.data.filter(x => x.show !== false)
  }

  find (id) {
    return this.data.find(el => el.id === id)
  }

  getAllData () {
    return this.data
  }
}
const TypesPaymentMethod = new Enumerate([
  { id: 'deposit', description: 'Depósito Bancario' },
  { id: 'transfer', description: 'Transferencia Bancaria' },
  { id: 'card', description: 'Tarjeta Crédito/Débito' },
])

const StatusPayment = new Enumerate([
  { id: 1, description: 'Pendiente de pago' },
  { id: 2, description: 'Pago realizado' },
  { id: 3, description: 'Pago verficado' },
  { id: 4, description: 'Pago verificado con tarjeta' },
])

const TypesSubjectSchema = new Enumerate([
  { id: '01', description: 'Persona Natural' },
  { id: '02', description: 'Persona Jurídica' },
])

const StatusUpcontaSchema = new Enumerate([
  { id: 'issued', description: 'Emitida' },
  { id: 'sent', description: 'Pendiente' },
  { id: 'draft', description: 'Borrador' },
  { id: 'canceled', description: 'Anulado' },
  { id: 'failed', description: 'Fallida' },
])

const TypeIdUpcontaSchema = new Enumerate([
  { id: '04', description: 'RUC' },
  { id: '05', description: 'Cédula' },
  { id: '06', description: 'Pasaporte' },
])

const TypeIds = new Enumerate([
  { id: '01', description: 'Cédula' },
  { id: '03', description: 'Pasaporte' },
])

const TypeRegister = new Enumerate([
  { id: '01', description: 'Medico extranjero', price: 200 },
  { id: '02', description: 'Socio', price: 60 },
  { id: '03', description: 'No socio', price: 150 },
  { id: '04', description: 'Postgrado / Residente socio', price: 30 },
  { id: '05', description: 'Postgrado / Residente no socio', price: 60 },
  { id: '06', description: 'Instrumentador', price: 30 },
  { id: '07', description: 'Fisoterapista', price: 30 },
  { id: '08', description: 'Estudiante', price: 30 },
])

const TypeModule = new Enumerate([
  { id: '01', description: 'Modulo de Rodilla', price: 60 },
  { id: '02', description: 'Modulo de Hombro', price: 60 },
  { id: '03', description: 'Modulo de Cadera', price: 60 },
  { id: '04', description: 'Modulo de Tobillo / Pie', price: 40 },
  { id: '05', description: 'Modulo Muñeca', price: 40 },
])

const TypeCourse = new Enumerate([
  { id: '01', description: 'Curso completo' },
  { id: '02', description: 'Curso por modulos específicos' },
])
export {
  TypesPaymentMethod,
  TypesSubjectSchema,
  StatusUpcontaSchema,
  StatusPayment,
  TypeIdUpcontaSchema,
  TypeRegister,
  TypeIds,
  TypeModule,
  TypeCourse,
}

export default Enumerate
