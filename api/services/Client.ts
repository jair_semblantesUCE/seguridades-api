import moment from 'moment'
import mail from '@sendgrid/mail'
import { TypesPaymentMethod, TypeModule, StatusUpcontaSchema, TypeRegister, TypeIdUpcontaSchema, TypeIds, TypeCourse } from '../libs/enumerate.js'
import Model, { ClientInter } from '../models/Clients'
import ModelPayment from '../models/CardPayments'
import ModelSequential from '../models/Sequential'
import { getBufferReport, isDev } from '../libs/utils'
import { getClientWelcome } from '../libs/email-templates'
import DocumentService from '../services/Documents'
import generateInvoice, { checkStatusInvoice } from '../libs/integrations/upconta'
import { postCreateLinkPayment, reversePaymentLink, statusLinkPayment } from '../libs/integrations/pagomedios'

const documentService = new DocumentService()
export default class Client {
  async rebilling (body: any) {
    if (!body._id) {
      throw new Error('No se recibio el id del cliente a refacturar')
    }
    const client:any = await Model.findOne({ _id: body._id })
    if (!client) {
      throw new Error('No se pudo encontrar el cliente para refacturar')
    }
    this.billingUpconta(body._id)
  }

  async getStats () {
    let status2 = 0
    let status3 = 0
    let deleted = 0
    const resp = await Model.find({ }, { payment: 1, deleted: 1 })
    for (const i of resp) {
      if (i.payment.status === 2) {
        status2++
      } else if (i.payment.status === 3 || i.payment.status === 4) {
        status3++
      } else if (i.deleted === true) {
        deleted++
      }
    }
    return {
      status2,
      status3,
      deleted,
    }
  }

  getCorrectPrice (type: string, course: string) {
    if (course === '01') {
      const types = TypeRegister.getData()
      const obj = types.find((item: any) => item.id === type)
      return obj.price
    } else {
      const types = TypeModule.getData()
      const obj = types.find((item: any) => item.id === type)
      return obj.price
    }
  }

  async createPaymentLink (data: any, client: string) {
    if (!data || !data.names || !data.surnames || !data.id || !data.email || !data.cellphone || !data.address || !client) {
      throw new Error('No se recibió los datos completos para crear su link de pago')
    }
    data.price = this.getCorrectPrice(data.typeRegister, data.typeCourse)
    data.fullname = `${data.names} ${data.surnames}`
    return await postCreateLinkPayment(data, client)
  }

  async reversePayment (data: any) {
    if (!data || !data.token) {
      throw new Error('Ingrese los datos necesarios para la anualción de su pago')
    }
    return await reversePaymentLink(data.token)
  }

  async clientReport (query:any) {
    if (!query || !query.startDate || !query.endDate) {
      throw new Error('No se recibio el rango de fechas para el reporte')
    }
    let startDate:any
    let endDate:any
    startDate = new Date(query.startDate)
    endDate = new Date(query.endDate)
    startDate.setHours(0, 0, 0, 0)
    endDate.setHours(23, 59, 59, 0)
    startDate = moment(startDate)
    endDate = moment(endDate)
    const { typesPayment, typesStatusUpconta, typesCourses, typesModules, typesRegister } = this.getOptions()
    const reports:any = await Model.find({
      _date: { $lte: endDate.toDate(), $gte: startDate.toDate() },
      'payment.status': { $gt: 1 },
    }).lean()
    reports.forEach((x:any) => {
      x.name = x.names + ' ' + x.surnames
      x._date = moment(x._date).format('DD-MM-YYYY')
      const course = typesCourses.find((y: any) => y.id === x.typeCourse)
      x.course = course.description
      if (x.typeCourse === '01') {
        const typeRegisterfirstcourse = typesRegister.find((y: any) => y.id === x.typeRegister)
        x.register = typeRegisterfirstcourse.description
        x.price = typeRegisterfirstcourse.price
      } else {
        const typeRegistersecondcourse = typesModules.find((y: any) => y.id === x.typeRegister)
        x.register = typeRegistersecondcourse.description
        x.price = typeRegistersecondcourse.price
      }
      const { description } = typesPayment.find((y: any) => y.id === x.payment.type)
      x.description = description
      if (x.infoBilling.billing) {
        const { description } = typesStatusUpconta.find((y: any) => y.id === x.infoBilling.billing.status)
        x.statusBilling = description
      }
      x.infoBilling.errs = x.infoBilling.errs.length > 0 ? x.infoBilling.errs.map(function (item: any) {
        return item.message + ''
      }) : x.infoBilling.errs.length === 0 && x.infoBilling.billing && x.infoBilling.billing.status === 'issued' ? 'Se facturó sin errores' : ''
    })

    const data = {
      reports,
      other: {
        endDate: endDate.format('DD-MM-YYYY'),
        startDate: startDate.format('DD-MM-YYYY'),
        generationDate: moment().format('DD-MM-YYYY LT'),
        total: reports.length,
      },
    }

    const filename = `reporte-clientes-del-${moment(startDate).format('DDMMYYYY')}-al-${moment(endDate).format('DDMMYYYY')}.xlsx`
    const path = '../assets/templates/ReportTemplate.xlsx'
    const xlsx = {
      data: getBufferReport(data, path) as any,
      name: `filename=${filename}`,
    }

    return xlsx
  }

  async post (body:ClientInter) {
    if (!body || !body.names || !body.surnames) {
      throw new Error('No se enviaron sus nombres completos')
    }
    if (!body || !body.email || !body.cellphone || !body.city || !body.address || !body.id) {
      throw new Error('Envie sus datos personales completos')
    }
    if (body.infoBilling.withSubject) {
      if (!body.infoBilling.name || !body.infoBilling.email || !body.infoBilling.phone || !body.infoBilling.city || !body.infoBilling.address || !body.infoBilling.id || !body.infoBilling.idType) {
        throw new Error('Envie sus datos de facturación completos')
      }
    }
    const client: ClientInter = await Model.create(body)
    await client.save()
    if (!client) {
      throw new Error('No se pudo completar el registro')
    }
    return { _id: client._id, message: 'success' }
  }

  async changeStusPayment (clientId: string, payment: any, update: boolean) {
    const client = await Model.findOne({ _id: clientId })
    if (!client) {
      throw new Error('No se pudo pudo registrar su pago, el cliente solicitado no existe')
    }
    if (payment.type === 'transfer' || payment.type === 'deposit') {
      if (!payment.document || !payment.document.base64 || !payment.document.name) {
        throw new Error('Envie su documento de validacion de pago')
      } else {
        const dataDocument = {
          client: clientId,
          name: payment.document.name,
        }
        const valid = await documentService.valueFile(payment.document.base64)
        if (!valid) {
          throw new Error('El tipo de archivo ingresado no es válido, por favor intente con documentos pdf, imágenes jpg o png')
        }
        const docId = await documentService.createDocument(dataDocument, payment.document.base64)
        if (!docId) {
          throw new Error('Se registró un inconveniente al guardar su comprobante de pago')
        }
        client.payment.type = payment.type
        if (!update) {
          client.payment.status = 2
          client.seq = await this.nextSequential('index')
        }
        client.payment.document = docId
        client.verified = true
        await client.save()
      }
    }
    if (payment.type === 'card') {
      // funcion de pagomedios para registrar el pago
      client.payment.type = payment.type
      if (!update) {
        client.payment.status = 3
        client.verified = true
        client.seq = await this.nextSequential('index')
      }
      await client.save()
    }
    return client._id
  }

  getOptions () {
    const data = {
      typesPayment: TypesPaymentMethod.getData(),
      typesIdUpconta: TypeIdUpcontaSchema.getData(),
      typesStatusUpconta: StatusUpcontaSchema.getData(),
      typesRegister: TypeRegister.getData(),
      typesIds: TypeIds.getData(),
      typesModules: TypeModule.getData(),
      typesCourses: TypeCourse.getData(),
    }
    return data
  }

  async getOne (idClient:string) {
    let document = {}
    if (!idClient) {
      throw new Error('No se envio un ID de cliente')
    }
    const _id = idClient
    const client = await Model.findOne({ _id })
    if (!client) {
      throw new Error('No existe un cliente con ese ID')
    }
    if (client.payment.type !== 'card' && client.payment.status > 1) {
      document = await documentService.getFile(client._id, client.payment.document)
      return { data: client, document }
    }
    return { data: client }
  }

  async get (query:any) {
    const resp = await (Model as any).smartQuery(query)
    const data = []
    for (const element of resp) {
      if (element.payment.status !== 1) {
        if (element.infoBilling && element.infoBilling.billing && element.infoBilling.billing.status) {
          const { description } = StatusUpcontaSchema.getData().find((x:any) => x.id === element.infoBilling.billing.status)
          element.infoBilling.billing.status = description
        }
        data.push(element)
      }
    }
    return { data, total: await (Model as any).smartCount(query) }
  }

  async delete (idClient:string) {
    if (!idClient) {
      throw new Error('No se envio un ID de cliente')
    }
    const _id = idClient
    const client = await Model.updateOne({ _id }, {
      $set: {
        payment: {
          status: 1,
        },
        deleted: true,
        verified: false,
      },
    })
    if (!client) {
      throw new Error('No existe el cliente que se desea eliminar')
    }
    const resp = { id: client._id, message: 'success' }
    return resp
  }

  async update (idClient:string, body:any) {
    if (!idClient) {
      throw new Error('No se envio un ID')
    }
    if (!body || !body.names || !body.surnames || !body.email || !body.cellphone ||
      !body.city || !body.address || !body.payment || !body.id) {
      throw new Error('No se envio todos los datos')
    }
    const client:any = await Model.findOneAndUpdate({ _id: idClient }, { $set: body })
    if (!client) {
      throw new Error('No existe el cliente al que desea editar')
    }
    if (!body.reBill) {
      if (body.changeDocument) {
        const valid = await documentService.valueFile(body.changeDocument.document.base64)
        if (!valid) {
          throw new Error('El tipo de archivo ingresado no es válido, por favor intente con documentos pdf, imágenes jpg o png')
        }
        await documentService.deleteFile(idClient, body.payment.document)
        await this.changeStusPayment(idClient, body.changeDocument, true)
      }
      if (body.payment.status === 3 && client.payment.status === 2) {
        this.createUser(idClient)
      }
    }
    return { id: client._id, message: 'success' }
  }

  async createUser (idClient:string) {
    const client = await Model.findOne({ _id: idClient })
    if (!client) {
      throw new Error('No existe el cliente al que se desea verificar')
    }
    const seq = await this.nextSequential('next')
    client.password = 'see' + seq
    client.username = 'seeuser' + seq
    await client.save()
    const [send] = await this.sendWelcomeEmail(client) as any
    if (send.statusCode >= 400) {
      throw new Error('No se pudo enviar el correo')
    }
    return { _id: client._id, message: 'success' }
  }

  sendWelcomeEmail (body:any) {
    return mail.send({
      from: { name: 'Sociedad Ecuatoriana de Ortopedia y Traumatología', email: 'seot@noreply.ec' },
      to: isDev ? body.email : [{ email: body.email }, { email: 'doviedo@cbs-ec.com' }],
      subject: 'Su registro se ha verificado con éxito',
      html: getClientWelcome({
        name: `${body.names} ${body.surnames}`,
        ci: body.id,
        username: body.username,
        password: body.password,
      }),
    })
  }

  async nextSequential (type: string) {
    const { seq } = await ModelSequential.findOneAndUpdate(
      { _id: type },
      { $inc: { seq: 1 as any } },
      { new: true },

    )
    if (!seq || seq === null || seq === undefined) {
      throw new Error('No se pudo avanzar el secuencial')
    }
    return seq
  }

  async billingUpconta (id:string) {
    const client = await Model.findOne({ _id: id })
    if (!client) {
      throw new Error('No se encontró el cliente para facturar')
    }
    try {
      if (!client.infoBilling.withSubject) {
        let typeUpconta = client.typeId
        switch (client.typeId) {
          case '01':
            typeUpconta = '05'
            break
          case '03':
            typeUpconta = '06'
            break
          default:
            typeUpconta = '04'
            break
        }
        client.infoBilling.idType = typeUpconta
      }
      if (!client.infoBilling.withSubject && (client.infoBilling.idType === '05' || client.infoBilling.idType === '04')) {
        client.infoBilling.idType = client.id.length === 13 ? '04' : '05'
      }
      if (client.infoBilling.withSubject && (client.infoBilling.idType === '05' || client.infoBilling.idType === '04')) {
        client.infoBilling.idType = client.infoBilling.id.length === 13 ? '04' : '05'
      }
      if (client.infoBilling.withSubject) {
        client.infoBilling.subjectType = client.infoBilling.idType === '04' ? '02' : '01'
        client.infoBilling.cellphone = client.infoBilling.phone
      } else {
        client.infoBilling.id = client.id
        client.infoBilling.subjectType = client.id.length < 11 ? '01' : '02'
        client.infoBilling.name = `${client.names} ${client.surnames}`
        client.infoBilling.city = client.city
        client.infoBilling.email = client.email
        client.infoBilling.cellphone = client.cellphone
        client.infoBilling.phone = client.cellphone
        client.infoBilling.address = client.address
      }
      client.infoBilling.errs = []
      const product = {
        formaPago: client.payment.type === 'card' ? '19' : '20',
        precioUnitario: this.getCorrectPrice(client.typeRegister, client.typeCourse),
        product: isDev ? 'FA05' : '007',
        descripcion: `Curso Ecuatoriano de artroscopía y cirugía preservador"`,
        cantidad: 1,
      }
      const { invoice, warning } = await generateInvoice(client.infoBilling, product as any)
      if (invoice) {
        client.infoBilling.billing = {
          id: invoice._id,
          number: `${invoice.infoTributaria.estab}-${invoice.infoTributaria.ptoEmi}-${invoice.infoTributaria.secuencial}`,
          status: invoice.status,
        }
      }
      if (warning) {
        client.infoBilling.errs.push({
          date: new Date(),
          code: warning.typeCode || warning.code,
          message: warning.message,
        })
      }
      const { status } = await checkStatusInvoice(invoice._id)
      if (status) {
        client.infoBilling.billing.status = status
      }
    } catch (err) {
      client.infoBilling.errs.push({
        date: new Date(),
        code: err.name === 'Error' ? 'WarningUpconta' : err.name,
        message: err.message,
      })
    } finally {
      await client.save()
    }
  }

  async savedPaymentCard (id: string, data: any) {
    if (!data || !data.token || !data.url || !id) {
      throw new Error('No se recibió la información completa del link de pago')
    }
    const auxPayment = await ModelPayment.findOne({ client: id })
    if (!auxPayment) {
      data.client = id
      const paymentData = await ModelPayment.create(data)
      await paymentData.save()
      return { id, message: 'Se añadio correctamente la información de pago' }
    } else {
      auxPayment.token = data.token
      auxPayment.url = data.url
      await auxPayment.save()
      return { id, message: 'Se edito la información de pago' }
    }
  }

  async verifiedPaymentCard (id: string) {
    if (!id) {
      throw new Error('No se recibió el id del link de pago')
    }
    const paymentData = await ModelPayment.findOne({ client: id })
    if (!paymentData) {
      throw new Error('El id solicitado para revisión no existe')
    }
    const payment = await statusLinkPayment(paymentData.token)
    const client = await Model.findOne({ _id: id })
    if (!client) {
      throw new Error('No existe el cliente a verificar')
    }
    if (payment.data.status === 'Autorizada' && client.payment.status === 1) {
      this.createUser(id)
      client.payment.status = 4
      client.seq = await this.nextSequential('index')
      client.verified = true
      client.payment.type = 'card'
      await client.save()
      this.billingUpconta(id)
    }
    return { payment, client: await Model.findOne({ _id: id }) }
  }

  async reSendEmail (body: any) {
    if (!body || !body.client || !body.email) {
      throw new Error('Es necesario enviar el cliente y el correo')
    }
    const client = await Model.findOne({ _id: body.client }, { username: 1, password: 1, id: 1, names: 1, surnames: 1 })
    if (!client || !client.username || !client.password) {
      throw new Error('Verifique el pago para obtener un usuario y contraseña')
    }
    return mail.send({
      from: { name: ' Sociedad Ecuatoriana de Ortopedia y Traumatología', email: 'seot@noreply.ec' },
      to: isDev ? body.email : [{ email: body.email }, { email: 'doviedo@cbs-ec.com' }],
      subject: 'Su registro se ha verificado con éxito',
      html: getClientWelcome({
        name: `${client.names} ${client.surnames}`,
        ci: client.id,
        username: client.username,
        password: client.password,
      }),
    })
  }
}
