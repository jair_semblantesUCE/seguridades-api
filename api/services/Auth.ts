import moment from 'moment'
import Administrator from '../models/Administrators'
import { hashThis } from '../libs/utils'

export default class Auth {
  async auth (data: any) {
    const { username } = data
    const password = hashThis(data.password)
    const userCounter = await Administrator.countDocuments({ username })
    if (userCounter < 1) {
      throw new Error(`No se encontró el administrador : ${data.username}`)
    }
    const admin = await Administrator.findOne({ username, password })
    if (!admin) {
      throw new Error('La contraseña es incorrecta')
    }
    const dateExpiration = moment().add('24', 'hours').toISOString()
    return { id: admin._id, expiration: dateExpiration, name: admin.name, email: admin.email }
  }
}
