import { TypesPaymentMethod, StatusPayment, StatusUpcontaSchema } from '../libs/enumerate.js'

export default class Admin {
  getOptions () {
    const data = {
      typesPayment: TypesPaymentMethod.getData(),
      statusPayment: StatusPayment.getData(),
      statusUpcontaSchema: StatusUpcontaSchema.getData(),
    }
    return data
  }
}
