import fs from 'fs'
import os from 'os'
import FileType from 'file-type'
import Model from '../models/documents'
import { bucket } from '../libs/utils'

export default class Client {
  async createDocument (data: any, file: any) {
    const document = new Model(data)
    if (!document) {
      throw new Error('No se pudo guardar su archivo, por favor intente nuevamente')
    }
    const isSavedBucked = await this.saveFile(document, file)
    if (!isSavedBucked) {
      throw new Error('No se pudo guardar su archivo en el bucked, por favor intente nuevamente')
    }
    await document.save()
    return document._id
  }

  async saveFile (data: any, base64: any) {
    const buffer = Buffer.from(base64, 'base64')
    const pathstorage = `medical/${data.client}/${data._id}`
    const pathTmp = os.tmpdir()
    const filepath = pathTmp + '/' + data.id
    const gstorageOptions = { destination: pathstorage, validation: 'crc32c' }
    fs.writeFileSync(filepath, buffer)
    const send = await bucket.upload(filepath, gstorageOptions)
    if (!send) {
      throw new Error('No se pudo guardar su documento')
    }
    fs.unlinkSync(filepath)
    return send
  }

  async getFile (client: any, id: any) {
    const documentoPathGstore = `medical/${client}/${id}`
    const file = bucket.file(documentoPathGstore)
    const [exists] = await file.exists()
    if (exists) {
      const doc = await Model.findById(id)
      if (doc) {
        const [buffer] = await file.download()
        const mime = await FileType.fromBuffer(buffer)
        return {
          mime: mime?.mime,
          b64: buffer.toString('base64'),
          name: doc.name,
        }
      } else {
        throw new Error('No se pudo encontrar el documento en la base')
      }
    } else {
      throw new Error('No se pudo encontrar el documento')
    }
  }

  async deleteFile (client: any, id: any) {
    const documentoPathGstore = `medical/${client}/${id}`
    const file = bucket.file(documentoPathGstore)
    const [exists] = await file.exists()
    if (exists) {
      bucket.file(documentoPathGstore).delete()
      const _id = id
      const deleted = await Model.findByIdAndDelete({ _id })
      if (!deleted) {
        throw new Error('No existe el document que se desea eliminar')
      }
      return { _id: id, message: 'success' }
    } else {
      throw new Error('No se pudo encontrar el documento')
    }
  }

  async valueFile (b64: string) {
    const { mime } = await FileType.fromBuffer(Buffer.from(b64, 'base64')) as any
    return (mime === 'image/jpeg' || mime === 'image/png' || mime === 'application/pdf')
  }
}
