/**
 * Obtiene la uri para la conexión con la base de datos. Si se utiliza la
 * variable de entorno USE_LOCAL_DB utiliza la base de datos local.
 */
function getDatabaseURI (): string {
  const mongo = {
    user: process.env.MONGO_ROOT_USERNAME || 'medical',
    pass: process.env.MONGO_ROOT_PASSWORD || 'WlmNLioMrQJFSw7jR4mf',
    host: process.env.MONGO_HOST || '104.154.36.81',
    port: process.env.MONGO_PORT || 48613,
    authSource: process.env.MONGO_AUTH_SOURCE || 'medical',
    db: process.env.MONGO_DATABASE || 'medical',
  }
  return process.env.USE_LOCAL_DB
    ? `mongodb://127.0.0.1:27017/${mongo.db}`
    : `mongodb://${mongo.user}:${mongo.pass}@${mongo.host}:${mongo.port}/${mongo.db}?authSource=${mongo.authSource}`
}

export const mongoUri = getDatabaseURI()
export const sendgridKey = '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'
