export enum Collections {
  ADMINISTRATOR = 'administrators',
  CLIENTS = 'clients',
  DOCUMENTS = 'documents',
  SEQUENTIALS = 'sequentials',
  PAYMENTSCARD = 'paymentscard'
}
