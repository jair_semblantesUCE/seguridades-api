import { Document, Schema, model, Types } from 'mongoose'
import mongooseSmartQuery from 'mongoose-smart-query'
import { Collections } from './utils'

interface Payments extends Document {
  client: Types.ObjectId;
  _date: Date;
  token: string;
  url: string;
}

const schema = new Schema<Payments>({
  client: { type: Types.ObjectId, required: true, ref: 'clients' },
  _date: { type: Date, default: Date.now },
  token: { type: String, required: true },
  url: { type: String, required: true },
})

schema.plugin(mongooseSmartQuery, {
  defaultFields: 'token',
  fieldsForDefaultQuery: 'client',
})

export default model(Collections.PAYMENTSCARD, schema)
