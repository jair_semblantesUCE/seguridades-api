import { Schema, model, Document } from 'mongoose'
import { Collections } from './utils'

interface Admin extends Document {
  username: string;
  password: string;
  email: string;
  name: string;
}

const schema = new Schema<Admin>({
  username: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true },
  name: { type: String, required: true },
})

export default model(Collections.ADMINISTRATOR, schema)
