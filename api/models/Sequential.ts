import { Document, Schema, model } from 'mongoose'
import { Collections } from './utils'

interface ISequential extends Document {
    _id: string;
    seq: number;
  }

const schema = new Schema<ISequential>({
  _id: { type: String, required: true },
  seq: { type: Number, required: true },
}, { _id: false })

export default model(Collections.SEQUENTIALS, schema)
