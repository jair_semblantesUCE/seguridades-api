import { Schema, model, Document, Types } from 'mongoose'
import mongooseSmartQuery from 'mongoose-smart-query'
import { StatusPayment, TypesPaymentMethod, TypeIdUpcontaSchema, StatusUpcontaSchema, TypesSubjectSchema, TypeIds } from '../libs/enumerate'
import { Collections } from './utils'

export interface Payment extends Types.Subdocument {
  type: string;
  status: number;
  document: string;
  token: string;
}

export interface InfoBilling extends Types.Subdocument {
  withSubject: boolean;
  id: string;
  idType: string;
  subjectType: string;
  name: string;
  phone: string;
  email: string;
  address: string;
  cellphone: string;
  optionalEmail: string;
  city: string;
  billing: Billing;
  errs: Array<ErrorBill>;
}
export interface Billing extends Types.Subdocument {
  id: string;
  number: string;
  status: string;
}
export interface ErrorBill extends Types.Subdocument {
  code: string;
  date: Date;
  message: string;
}

export interface ClientInter extends Document {
  ci: string;
  names: string;
  surnames: string;
  email: string;
  cellphone: string;
  city: string;
  address: string;
  payment: Payment;
  infoBilling: InfoBilling;
  seq: String;
  _date: Date;
  deleted: boolean;
  verified: boolean;
  typeRegister: string;
  typeCourse: string;
  typeId: string;
}
const errsSchema = new Schema<ErrorBill>({
  code: String,
  date: Date,
  message: String,
}, { _id: false })

const schemaInvoice = new Schema<InfoBilling>({
  withSubject: { type: Boolean, default: true },
  id: { type: String, required: false },
  errs: [errsSchema],
  subjectType: { type: String, enum: TypesSubjectSchema.getEnum(), required: false },
  idType: {
    type: String,
    enum: TypeIdUpcontaSchema.getEnum(),
    required: false,
  },
  billing: {
    id: Types.ObjectId,
    number: String,
    status: { type: String, enum: StatusUpcontaSchema.getEnum() },
  },
  name: { type: String, required: false },
  email: { type: String, required: false, lowercase: false },
  optionalEmail: { type: String, required: false, lowercase: false },
  phone: { type: String, required: false },
  cellphone: { type: String, required: false },
  city: { type: String, required: false },
  address: { type: String, required: false },
}, { _id: false })

const schemaPayment = new Schema<Payment>({
  type: { type: String, enum: TypesPaymentMethod.getEnum(), required: false },
  status: { type: Number, enum: StatusPayment.getEnum(), default: '1' },
  document: { type: Types.ObjectId, ref: 'documents', required: false },
  token: { type: String, required: false },
}, { _id: false })

const schemaClient = new Schema<ClientInter>({
  id: { type: String, required: true },
  names: { type: String, required: true },
  surnames: { type: String, required: false },
  email: { type: String, required: true },
  cellphone: { type: String, required: true },
  city: { type: String, required: true },
  address: { type: String, required: true },
  payment: { type: schemaPayment, required: true },
  _date: { type: Date, default: Date.now },
  username: { type: String, required: false },
  password: { type: String, required: false },
  seq: { type: String, required: false },
  deleted: { type: Boolean, default: false },
  verified: { type: Boolean, default: false },
  typeRegister: { type: String, required: true },
  typeCourse: { type: String, required: true },
  typeId: { type: String, required: true, enum: TypeIds.getEnum() },
  infoBilling: { type: schemaInvoice, default: {} },
})

schemaClient.plugin(mongooseSmartQuery, {
  defaultFields: 'id',
  fieldsForDefaultQuery: 'names surnames id',
})

export default model(Collections.CLIENTS, schemaClient)
