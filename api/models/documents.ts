import { Document, Schema, model, Types } from 'mongoose'
import mongooseSmartQuery from 'mongoose-smart-query'
import { Collections } from './utils'

interface Documents extends Document {
  client: Types.ObjectId;
  _date: Date;
  name: string;
}

const schema = new Schema<Documents>({
  client: { type: Types.ObjectId, required: true, ref: 'clients' },
  _date: { type: Date, default: Date.now },
  name: { type: String, required: true },
})

schema.plugin(mongooseSmartQuery, {
  defaultFields: 'name',
  fieldsForDefaultQuery: 'name',
})

export default model(Collections.DOCUMENTS, schema)
