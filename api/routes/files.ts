import express from 'express'
import Controller from '../controllers/client'

const router = express.Router()
router.get('/get-file', Controller.getFile)

export default router
