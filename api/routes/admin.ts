import express from 'express'
import Controller from '../controllers/admin'
import { adminMiddleware } from '../controllers/auth'

const router = express.Router()
router.use(adminMiddleware)
router.get('/get-options', Controller.getOptions)

export default router
