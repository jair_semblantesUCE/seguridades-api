import express from 'express'
import Controller from '../controllers/client'
import { adminMiddleware } from '../controllers/auth'

const router = express.Router()
// router.use(adminMiddleware)
router.get('', adminMiddleware, Controller.get)
router.get('/stats', Controller.getStats)
router.get('/get-options', Controller.getOptions)
router.post('/false-delete/:id', Controller.delete)
router.post('/saved-payment-card/:id', Controller.savedPaymentCard)
router.post('/verified-payment-card/:id', Controller.verifiedPaymentCard)
router.post('/generate-link', Controller.createPaymentLink)
router.post('/reverse-payment', Controller.reversePayment)
router.post('/register-payment/:id', Controller.changeStusPayment)
router.post('/rebilling', Controller.rebilling)
router.get('/report', Controller.clientReport)
router.get('/:id', adminMiddleware, Controller.getOne)
router.get('/get-file', Controller.getFile)
router.post('', Controller.post)
router.post('/re-send', adminMiddleware, Controller.reSendEmail)
router.put('/:id', adminMiddleware, Controller.update)

export default router
