import express from 'express'
import Controller from '../controllers/auth'

const router = express.Router()

router.get('', Controller.getAuth)
router.post('', Controller.login)

export default router
