import express from 'express'
import Auth from './auth'
import Client from './client'
import Admin from './admin'
import Files from './files'

const router = express.Router()

router.use('/auth', Auth)
router.use('/clients', Client)
router.use('/admins', Admin)
router.use('/file', Files)

export default router
