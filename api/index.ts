import { connect, connection } from 'mongoose'
import express, { NextFunction, Request, Response } from 'express'
import morgan from 'morgan'
import sgMail from '@sendgrid/mail'
import Routes from './routes'
import { mongoUri, sendgridKey } from './config'

const app = express()

sgMail.setApiKey(sendgridKey)

app.use(morgan('dev'))

app.use(express.json({ limit: '50mb' }))

app.use(express.urlencoded({ limit: '50mb' }))

console.info(`Conectado a base de datos ${mongoUri}`)
connect(mongoUri, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
connection.on('error', (err) => {
  console.error('error conectando a la base de datos', err)
  process.exit(1)
})
connection.once('open', () => {
  app.use(Routes)
  app.use(function (err: Error, req: Request, res: Response, _next: NextFunction) {
    console.error(`Path: ${req.path}\n`, err.stack)
    res.status(500).send({ message: err.message, error: 'Ocurrio un error' })
  })
})

export default app
